package br.com.zipkin.resultadocep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ResultadoCepApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResultadoCepApplication.class, args);
	}

}
