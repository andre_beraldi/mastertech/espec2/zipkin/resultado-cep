package br.com.zipkin.resultadocep.service;

import br.com.zipkin.resultadocep.client.ConsultaCepClient;
import br.com.zipkin.resultadocep.client.ConsultaCepDTO;
import br.com.zipkin.resultadocep.model.RequisicaoCep;
import br.com.zipkin.resultadocep.model.dto.ResultadoRequisicaoCepDto;
import br.com.zipkin.resultadocep.model.mapper.ResultadoCepMapper;
import com.ctc.wstx.sr.CompactNsContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

@Service
public class ResultadoCepService {
    @Autowired
    private ConsultaCepClient consultaCepClient;

    @NewSpan(name = "buscar-logradouro-service")
    public ConsultaCepDTO buscarLogradouro(RequisicaoCep requisicaoCep){
        return consultaCepClient.ConsultarCep(requisicaoCep.getCep());
    }
}
