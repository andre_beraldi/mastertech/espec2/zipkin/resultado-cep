package br.com.zipkin.resultadocep.client;

public class ConsultaCepDTO {
    private String logradouro;

    public ConsultaCepDTO() {
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
}
