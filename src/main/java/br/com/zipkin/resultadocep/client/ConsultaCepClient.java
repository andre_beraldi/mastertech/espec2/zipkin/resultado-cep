package br.com.zipkin.resultadocep.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "consultacep")
public interface ConsultaCepClient {

    @GetMapping("/consulta-cep/{cep}")
    ConsultaCepDTO ConsultarCep(@PathVariable String cep);
}
