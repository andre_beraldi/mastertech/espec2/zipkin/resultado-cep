package br.com.zipkin.resultadocep.model;

public class RequisicaoCep {
    private String nomeUsuario;
    private String cep;

    public RequisicaoCep() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
