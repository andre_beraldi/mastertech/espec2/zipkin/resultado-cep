package br.com.zipkin.resultadocep.model.dto;

public class ResultadoRequisicaoCepDto {
    private String nomeUsuario;
    private String logradouro;

    public ResultadoRequisicaoCepDto() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
}
