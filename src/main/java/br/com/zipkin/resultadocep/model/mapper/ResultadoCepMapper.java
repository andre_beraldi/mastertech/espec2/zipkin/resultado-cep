package br.com.zipkin.resultadocep.model.mapper;

import br.com.zipkin.resultadocep.client.ConsultaCepDTO;
import br.com.zipkin.resultadocep.model.RequisicaoCep;
import br.com.zipkin.resultadocep.model.dto.ResultadoRequisicaoCepDto;
import org.springframework.stereotype.Component;

@Component
public class ResultadoCepMapper {

    public ResultadoRequisicaoCepDto toGetResponse(ConsultaCepDTO consultaCepDTO, RequisicaoCep requisicaoCep) {
        ResultadoRequisicaoCepDto resultadoRequisicaoCepDto = new ResultadoRequisicaoCepDto();

        resultadoRequisicaoCepDto.setNomeUsuario(requisicaoCep.getNomeUsuario());
        resultadoRequisicaoCepDto.setLogradouro(consultaCepDTO.getLogradouro());

        return resultadoRequisicaoCepDto;
    }


}
