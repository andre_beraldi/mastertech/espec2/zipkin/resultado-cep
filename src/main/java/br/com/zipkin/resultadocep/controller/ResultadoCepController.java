package br.com.zipkin.resultadocep.controller;

import br.com.zipkin.resultadocep.client.ConsultaCepDTO;
import br.com.zipkin.resultadocep.model.RequisicaoCep;
import br.com.zipkin.resultadocep.model.dto.ResultadoRequisicaoCepDto;
import br.com.zipkin.resultadocep.model.mapper.ResultadoCepMapper;
import br.com.zipkin.resultadocep.service.ResultadoCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.reactive.context.ReactiveWebApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResultadoCepController {

    @Autowired
    private ResultadoCepService resultadoCepService;

    @Autowired
    private ResultadoCepMapper resultadoCepMapper;

    @GetMapping("/resultado-cep/{nome}/{cep}")
    public ResultadoRequisicaoCepDto buscarLogradouro(@PathVariable String nome, @PathVariable String cep){
        RequisicaoCep requisicaoCep = new RequisicaoCep();
        requisicaoCep.setNomeUsuario(nome);
        requisicaoCep.setCep(cep);

        ConsultaCepDTO consultaCepDTO = resultadoCepService.buscarLogradouro(requisicaoCep);

        return resultadoCepMapper.toGetResponse(consultaCepDTO, requisicaoCep);
    }
}
